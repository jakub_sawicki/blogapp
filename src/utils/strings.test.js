import { truncateString } from './strings';

describe('Truncate String util function', () => {
    let testString, threshold, result, expected;

    it('should work properly if string length greater than threshold', () => {
        testString = 'qwertyuiop';
        threshold = 6;
        result = truncateString(testString, threshold);
        expected = 'qwerty...';
        expect(result).toBe(expected);
    })

    it('should work properly if string length less than threshold', () => {
        testString = 'qwerty';
        threshold = 16;
        result = truncateString(testString, threshold);
        expected = 'qwerty';
        expect(result).toBe(expected);
    })

    it('should work properly if string length equals threshold', () => {
        testString = 'qwerty';
        threshold = 6;
        result = truncateString(testString, threshold);
        expected = 'qwerty';
        expect(result).toBe(expected);
    })
})

