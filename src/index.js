import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import HeaderContainer from './containers/Header/HeaderContainer';
import PostsContainer from './containers/Posts/PostsContainer';
import style from './styles/styles.scss';
import PostDetailsContainer from './containers/PostDetails/PostDetailsContainer';

/* SIMPLIFIED VERSION */
const Content = () => (
    <Switch>
        <Route exact path='/home' render={() => (<h1>HOME</h1>)}/>
        <Route exact path='/posts' component={PostsContainer}/>
        <Route exact path='/posts/:id' render={
            ({match}) => (<PostDetailsContainer postId={match.params.id}/>)}/>
    </Switch>
)

const MainLayout = () => (
    <Fragment>
        <HeaderContainer/>
        <Content/>  
    </Fragment>
)

const App = () => (
    <Provider store={store}>
        <Router>
            <MainLayout/>
        </Router>
    </Provider>
)

ReactDOM.render(<App/>, document.getElementById('root'));


