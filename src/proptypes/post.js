import { shape, number, string } from 'prop-types';

export default shape({
    userId: number.isRequired,
    id: number.isRequired,
    title: string.isRequired,
    body: string.isRequired
})