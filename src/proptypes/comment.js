import { shape, number, string } from 'prop-types';

export default shape({
    postId: number.isRequired,
    id: number.isRequired,
    name: string.isRequired,
    email: string.isRequired,
    body: string.isRequired
})