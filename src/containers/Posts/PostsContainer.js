import React, { Component } from 'react';
import { arrayOf, number, func } from 'prop-types';
import PostPropType from '../../proptypes/post';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import CommentsContainer from '../Comments/CommentsContainer';
import PostList from '../../components/Posts/PostList/PostList';
import Pagination from '../../components/common/Pagination/Pagination';
import { getPosts } from '../../store/posts/actions';
import { updateSelectedPage } from '../../store/pagination/actions';
import { selectedPostsSelector } from '../../store/posts/selectors';
import { selectedPageSelector } from '../../store/pagination/selectors';
import PostDetails from '../../components/Posts/PostDetails/PostDetails';
import BasicLayout from '../../components/common/BasicLayout/BasicLayout';


export class PostsContainer extends Component {
    static propTypes = {
        posts: arrayOf(PostPropType).isRequired,
        selectedPage: number.isRequired,
        getPosts: func.isRequired,
        updateSelectedPage: func.isRequired
    }

    componentDidMount() {
        const { posts, getPosts, selectedPage } = this.props;
        if (posts.length === 0) {
            //console.log('fetching posts on mount...')
            getPosts(selectedPage);
        }
    }
    componentDidUpdate(prevProps) {
        const { getPosts, selectedPage } = this.props;
        if (selectedPage !== prevProps.selectedPage) {
            //console.log('fetching posts on update...')
            getPosts(selectedPage);
        }
    }

    render() {
        const { posts, selectedPage, updateSelectedPage } = this.props;
        return (
            <BasicLayout>
                <Pagination listSize={100} limit={10} selected={selectedPage} onSelect={updateSelectedPage} />
                <PostList posts={posts} />             
            </BasicLayout>
        )
    }
    /*
        <Fragment>
                <Route exact path='/posts' render={() => (
                    <Fragment>
                        <Pagination listSize={100} limit={10} selected={selectedPage} onSelect={updateSelectedPage} />
                        <PostList posts={posts} />
                    </Fragment>
                )} />
                <Route exact path='/posts/:id' render={({ match }) => (
                    <Fragment>
                        <PostDetails post={posts.find(x => x.id == match.params.id)} />
                        <CommentsContainer id={match.params.id} />
                    </Fragment>
                )} />
            </Fragment>

    



        <Route /posts/:id render => {
            <PostDetailsContainer postId={match ....}>
                <PostDetails props={fetchedPost}/>
                <CommentList comments={fetchedComments}>
            </PostDetailsContainer>
        }

        */
        /*

        =================================
        ----->   route /posts

        <PostsContainer>
            <PostList>
                <Post/>
                <Post/>
                <Post/>
            </PostList>
            <Pagination>
        </PostsContainer>

        -----> route /posts:id

        <PostDetailsContainer postId={match ....}>
            <PostDetails props={fetchedPost}/>
            <CommentList comments={fetchedComments}>
        </PostDetailsContainer>
    */
}

const mapStateToProps = (state) => ({
    posts: selectedPostsSelector(state),
    selectedPage: selectedPageSelector(state)
});

const mapDispatchToProps = {
    getPosts,
    updateSelectedPage
};

export default connect(mapStateToProps, mapDispatchToProps)(PostsContainer);



