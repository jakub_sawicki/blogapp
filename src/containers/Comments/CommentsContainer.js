import React, { Component } from 'react';
import { string } from 'prop-types';
import { connect } from 'react-redux';
import CommentList from '../../components/Comments/CommentList/CommentList';
import Pagination from '../../components/common/Pagination/Pagination';
import { getComments, resetComments } from '../../store/comments/actions';
import { selectedCommentsSelector } from '../../store/comments/selectors';

export class CommentsContainer extends Component {
    static propTypes = {
        id: string
    }

    state = {
        selectedPage: 1
    }

    componentDidMount() {
        const { getComments, id } = this.props;
        getComments(id);
    }

    componentWillUnmount() {
        this.props.resetComments();
    }

    updateSelectedPage = (newPage) => {
        this.setState({
            selectedPage: newPage
        })
    }

    render() {
        const { selectedPage } = this.state;
        const { comments } = this.props;
        const from = (selectedPage - 1) * 2;
        const commentsOnPage = comments.slice(from, from + 2);
        return (
            <div>
                <Pagination listSize={comments.length} limit={2} selected={selectedPage} onSelect={this.updateSelectedPage}/>
                <CommentList comments={commentsOnPage}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
   comments: selectedCommentsSelector(state)
});

const mapDispatchToProps = {
    getComments,
    resetComments
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentsContainer);

