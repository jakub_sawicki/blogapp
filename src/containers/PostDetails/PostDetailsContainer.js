import React, { Component } from 'react';
import { string } from 'prop-types';
import BasicLayout from '../../components/common/BasicLayout/BasicLayout';
import Pagination from '../../components/common/Pagination/Pagination';
import CommentList from '../../components/Comments/CommentList/CommentList';
import PostDetails from '../../components/Posts/PostDetails/PostDetails';


export class PostDetailsContainer extends Component {
    static propTypes = {
        postId: string.isRequired
    }

    state = {
        post: undefined,
        comments: [],
        selectedPage: 1
    }

    componentDidMount() {
        this.fetchPost();
        this.fetchComments();
    }

    updateSelectedPage = (newPage) => {
        this.setState({
            selectedPage: newPage
        })
    }


    fetchPost = () => {
        fetch(`https://jsonplaceholder.typicode.com/posts?id=${this.props.postId}`)
        .then(response => response.json())
        .then(data => {
            this.setState({
                post: data[0]
            })
        })
        .catch(error => {
            console.log(error);
        })
    }

    fetchComments = () => {
        fetch(`https://jsonplaceholder.typicode.com/comments?postId=${this.props.postId}`)
        .then(response => response.json())
        .then(data => {
            this.setState({
                comments: data
            })
        })
        .catch(error => {
            console.log(error)
        })
    }

    render() {
        const { selectedPage, comments, post } = this.state;
        const from = (selectedPage - 1) * 2;
        const commentsOnPage = comments.slice(from, from + 2);
        return (
            <BasicLayout>
                <PostDetails post={post} />
                <Pagination listSize={comments.length} limit={2} selected={selectedPage} onSelect={this.updateSelectedPage}/>
                <CommentList comments={commentsOnPage}/>
            </BasicLayout>
        )
    }
   
}



export default PostDetailsContainer;



