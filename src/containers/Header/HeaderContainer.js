import React, { Component } from 'react';
import { number, func } from 'prop-types';
import { connect } from 'react-redux';
import Navbar from '../../components/Header/Navbar/Navbar';
import { getNumberOfPosts } from '../../store/posts/actions';

export class HeaderContainer extends Component {
    static propTypes = {
        getNumberOfPosts: func,
        numberOfPosts: number
    }

    componentDidMount() {
        this.props.getNumberOfPosts();
    }

    render() {
        return (
            <Navbar numberOfPosts={this.props.numberOfPosts} />
        )
    }
}

const mapStateToProps = (state) => ({
    numberOfPosts: state.posts.numberOfPosts
});

const mapDispatchToProps = {
    getNumberOfPosts
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);

