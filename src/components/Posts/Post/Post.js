import React from 'react';
import { Link } from 'react-router-dom';
import style from './styles.scss';
import { truncateString } from '../../../utils/strings';
import PostPropType from '../../../proptypes/post';
import Label from '../../common/Label/Label';

export const Post = ({post}) => {
    const body = truncateString(post.body, 150);
    const title = truncateString(post.title, 15)
    return (
        <div className={style.container}>
            <img src={post.imgSrc} alt="" className={style.image}/>
            <div className={style.section}>
                <span className={style['section__title']}>{title}</span>
                <Label value={post.numberOfComments} text="comments"/>
                <p className={style['section__body']}>{body}</p>
                <Link to={`posts/${post.id}`} className={style['section__link']}>read more</Link> 
            </div>
        </div>
    )
}

Post.propTypes = {
    post: PostPropType.isRequired
}

export default Post;