import React from 'react';
import style from './styles.scss';
import PostPropType from '../../../proptypes/post';
import { Post } from '../Post/Post';

export const PostDetails = ({post}) => {
    return (
        <div className={style.container}>
            <h1>{post.title}</h1>
            <hr/>
            <img src={post.imgSrc}/>
            <p>{post.body}</p>
        </div>
    )
}

PostDetails.propTypes = {
    post: PostPropType
}

PostDetails.defaultProps = {
    post: {
        id: 0,
        userId: 0,
        title: '',
        body: ''
    }
}

export default PostDetails;