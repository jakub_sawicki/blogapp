import React from 'react';
import { arrayOf } from 'prop-types';
import PostPropType from '../../../proptypes/post';
import Post from '../Post/Post';
import style from './styles.scss';

export const PostList = ({posts}) => (
    <div className={style.container}>
        <h2>Recent Posts</h2>
        <hr></hr>
        {posts.map(x => (
            <Post key={x.id} post={x}/>
        ))}
    </div>
)

PostList.propTypes = {
    posts: arrayOf(PostPropType).isRequired
}


export default PostList;


//  <Post key={x.id} post={x}/>