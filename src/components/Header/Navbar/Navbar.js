import React from 'react';
import { Link } from 'react-router-dom';
import { number } from 'prop-types';
import style from './styles.scss';
import Badge from '../../common/Badge/Badge';

const Navbar = ({numberOfPosts}) =>  (
    <div className={style.container}>
        <Link to="/home" className={`${style.link} ${style['link--big']}`}>Blog App</Link>
        <ul className={style.list}>
            <li>
                <Link to="/posts" className={`${style.link} ${style['link--small']}`}>Posts</Link>
                <Badge value={numberOfPosts}/>
            </li>
        </ul>
    </div>
)


Navbar.propTypes = {
    numberOfPosts: number.isRequired
};

export default Navbar;