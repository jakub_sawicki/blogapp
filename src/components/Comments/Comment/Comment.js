import React from 'react';
import style from './styles.scss';
import CommentPropType from '../../../proptypes/comment';

export const Comment = ({comment}) => {
    return (
        <div className={style.container}>
            <span className={style.name}>{comment.name}</span>
            <span className={style.email}>{comment.email}</span>
            <p className={style.body}>{comment.body}</p>
        </div>
    )
}

Comment.propTypes = {
    comment: CommentPropType.isRequired
}

export default Comment;