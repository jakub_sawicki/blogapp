import React from 'react';
import { arrayOf } from 'prop-types';
import CommentPropType from '../../../proptypes/comment';
import Comment from '../Comment/Comment';
import style from './styles.scss';

export const CommentList = ({comments}) => (
    <div className={style.container}>
        <h2>Comments</h2>
        <hr></hr>
        {comments.map(x => (   
            <Comment key={x.id} comment={x}/>
        ))}
    </div>
)

CommentList.propTypes = {
    comments: arrayOf(CommentPropType).isRequired
}


export default CommentList;