import React from 'react';
import Pagination from './Pagination';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import '../../../tests/setup';

//import renderer from 'react-test-renderer';

describe('Pagination', () => {
    const createShallow = () => shallow(<Pagination {...mockProps}/>)

    let mockProps;

    beforeEach(() => {
        mockProps = {
            listSize: 100,
            limit: 10,
            selected: 5,
            onSelect: jest.fn()
        };
    })
    
    it('should render properly', () => {
        const wrapper = createShallow();
        expect(toJson(wrapper)).toMatchSnapshot();
    })
})

/*
describe('Pagination', () => {
    let props;

    const createPagination = () => renderer.create(<Pagination {...props}/>)
    
    beforeEach(() => {
        props = {
            listSize: 100,
            limit: 10,
            selected: 5,
            onSelect: (value) => {
                if (value >= 1 && value <= this.pageCount) {
                    this.selected = value;
                }
            }
        };
    })

    it('should match the snapshot', () => {
        const tree = createPagination().toJSON();
        expect(tree).toMatchSnapshot();
    })

    it('should match the snapshot after click', () => {
        
        const rendered = createPagination();
        let tree = rendered.toJSON();
        expect(tree).toMatchSnapshot();
        
        tree.props.onSelect()
        tree = rendered.toJSON();
        expect(tree).toMatchSnapshot();
        
    })
})
*/