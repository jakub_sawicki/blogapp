import React from 'react';
import { number, func } from 'prop-types';
import style from './styles.scss';

export const Pagination = ({ listSize, limit, selected, onSelect }) => {
    const pageCount = Math.ceil(listSize / limit);
    const range = Array.from({length: pageCount}, (x, i) => i + 1); // [1, 2, 3, 4.....]

    const handleSelect = (value) => (e) => {
        if (value >= 1 && value <= pageCount) {
            onSelect(value);
        }
    }
    
    const isActive = (i) => {
        const itemStyle = style['list__item']
        const activeStyle = style['active']
        if (i === selected) {
            return `${itemStyle} ${activeStyle}`;
        } else {
            return `${itemStyle}`;
        }
    }

    const numberListItems = (
        range.map((x, i) => (
            <li onClick={handleSelect(x)} key={i} className={isActive(i + 1)}>{x}</li>
        ))
    )

    return (
        <ul className={style.list}>
            <li onClick={handleSelect(selected - 1)} className={style['list__item--first']}>&#8592;</li>
            {numberListItems}
            <li onClick={handleSelect(selected + 1)} className={style['list__item--last']}>&#8594;</li>
        </ul>
    )
}
    
Pagination.propTypes = {
    listSize: number.isRequired,
    limit: number.isRequired,
    selected: number.isRequired,
    onSelect: func.isRequired
}

export default Pagination;