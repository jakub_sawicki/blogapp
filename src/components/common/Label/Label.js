import React, { Fragment } from 'react';
import { number, string } from 'prop-types';
import style from './styles.scss';

export const Label = ({value, text}) => {
    let item;
    if (value > 0) {
        item = (
            <span className={`${style.container} ${style['container--red']}`}>
                {`${value} ${text}`}
            </span>
        )
    } else {
        item = (
            <span className={`${style.container} ${style['container--green']}`}>
                {`no ${text}`}
            </span>
        )
    }
    return (
        <Fragment>
            {item}
        </Fragment>
    )
}
    
Label.propTypes = {
    value: number, 
    text: string
}

export default Label;