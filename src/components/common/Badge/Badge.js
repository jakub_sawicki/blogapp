import React from 'react';
import { number } from 'prop-types';
import style from './styles.scss';

const Badge = ({ value }) => (
    <span className={style.container}>{value}</span>
)

Badge.propTypes = {
    value: number.isRequired
}

export default Badge;