import React from 'react';
import Badge from './Badge';
import style from './styles.scss';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import '../../../tests/setup';

//import renderer from 'react-test-renderer';

describe('Badge', () => {
    const createShallow = () => shallow(<Badge {...mockProps}/>)

    let mockProps;

    beforeEach(() => {
        mockProps = {
            number: 0
        }
    })

    it('should render properly', () => {
        const wrapper = createShallow();
        expect(toJson(wrapper)).toMatchSnapshot();
    })
})

/*
describe('Badge', () => {
    let props;

    const createBadge = () => renderer.create(<Badge {...props}/>)
    
    beforeEach(() => {
        props = {
            number: 0
        };
    })

    it('should match the snapshot', () => {
        const tree = createBadge().toJSON();
        expect(tree).toMatchSnapshot()
    })

    
    
    it('should render properly', () => {
        const badge = shallowBadge();
        //expect(badge.html()).toBe(`<span>${props.number}</span>`);
        console.log(badge.html());
    })
    

    it('should render only one span', () => {
        const badge = shallowBadge();
        const spansFound = badge.find('span');
        expect(spansFound).toHaveLength(1);
    })

    it('should have a prop "number" equal or greater than 0', () => {
        const badge = mountedBadge();
        expect(badge.props().number).toBeGreaterThanOrEqual(0);
    })

    it('should receive only one prop', () => {
        const badge = mountedBadge();
        const receivedProps = badge.props();
        const keys = Object.keys(receivedProps);
        expect(keys).toHaveLength(1);
    })

    it ('should have span with class container', () => {
        const badge = shallowBadge();
        //console.log(style.container);
    })
    
})


*/