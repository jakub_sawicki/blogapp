import React from 'react';
import style from './styles.scss'

export const BasicLayout = ({ children }) => (
    <div className={style.container}>
        {children}
    </div>
)

export default BasicLayout;