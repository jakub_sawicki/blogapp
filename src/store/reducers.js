import { combineReducers } from 'redux'
import postsReducer from './posts';
import paginationReducer from './pagination';
import commentsReducer from './comments';

export default combineReducers({
    posts: postsReducer,
    pagination: paginationReducer,
    //comments: commentsReducer
})