import { UPDATE_SELECTED_PAGE } from './types';

const initialState = {
    selectedPage: 1
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_SELECTED_PAGE: {
            return {
                ...state,
                selectedPage: action.payload.page
            }
        }
        default: {
            return state
        }
    }
}

export default reducer;
