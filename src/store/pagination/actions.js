import { UPDATE_SELECTED_PAGE } from './types';

export const updateSelectedPage = (pageNumber) => (dispatch) => {
    dispatch({
        type: UPDATE_SELECTED_PAGE,
        payload: {
            page: pageNumber
        }
    })   
}