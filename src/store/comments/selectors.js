import { createSelector } from 'reselect';

const commentsSelector = (state) => state.comments.byId;
const selectedIdsSelector = (state) => state.comments.allIds; 

const getSelectedComments = (byId, allIds) => {
    const comments = [];
    for (const id of allIds) {
        comments.push(byId[id]);
    }
    return comments;
}

export const selectedCommentsSelector = createSelector(
    commentsSelector,
    selectedIdsSelector,
    getSelectedComments
)