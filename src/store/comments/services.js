export const CommentsService = {
    url: 'https://jsonplaceholder.typicode.com/comments',
    getComments() {
        return fetch(this.url)
            .then(response => response.json())
            .catch(error => console.log(error));
    },
    getCommentsById(id) {
        return fetch(`${this.url}?postId=${id}`)
            .then(response => response.json())
            .catch(error => console.log(error));
    }
}
