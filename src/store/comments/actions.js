import { GET_COMMENTS, RESET_COMMENTS } from './types';
import { CommentsService } from './services';

export const getComments = (id) => (dispatch) => {
    CommentsService.getCommentsById(id).then(data => {
        const commentsById = {};
        for(const comment of data) {
            commentsById[comment.id] = comment;
        }
        dispatch({
            type: GET_COMMENTS,
            payload: {
                commentsById: commentsById,
                allIds: Object.keys(commentsById).map(x => Number(x))
            }
        })
    })
}

export const resetComments = () => (dispatch) => {
    dispatch({
        type: RESET_COMMENTS
    })
}