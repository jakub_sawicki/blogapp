import { GET_COMMENTS, RESET_COMMENTS } from './types';

const initialState = {
    byId: {},
    allIds: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_COMMENTS: {
            return {
                ...state,
                byId: action.payload.commentsById,
                allIds: action.payload.allIds
            }
        }
        case RESET_COMMENTS: {
            return initialState;
        }
        default: {
            return state;
        }
    }
}

export default reducer;