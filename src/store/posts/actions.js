import { GET_POSTS, GET_NUMBER_OF_POSTS } from './types';
import { PostsService, CommentsService } from './services';

export const getPosts = (page) => (dispatch) => {
    PostsService.getPosts(page).then(data => {
        const postsById = {};
        for(const post of data) {
            post.numberOfComments = 5;
            post.imgSrc = 'https://via.placeholder.com/64x64';
            postsById[post.id] = post;
        }
        dispatch({
            type: GET_POSTS,
            payload: {
                postsById: postsById,
                allIds: Object.keys(postsById).map(x => Number(x))
            }
        })
    });  
}

export const getNumberOfPosts = () => (dispatch) => {
    PostsService.getNumberOfPosts().then(data => {
        dispatch({
            type: GET_NUMBER_OF_POSTS,
            payload: {
                numberOfPosts: data
            }
        })
    });
}