export const GET_POSTS = 'GET_POSTS';
export const GET_COMMENTS = 'GET_COMMENTS';
export const GET_NUMBER_OF_POSTS = 'GET_NUMBER_OF_POSTS';
export const GET_POST_IDS = 'GET_POST_IDS';