export const PostsService = {
    url: 'https://jsonplaceholder.typicode.com/posts',
    getPosts(page) {
        return fetch(this.url + `?_page=${page}`)
            .then(response => response.json())
            .catch(error => console.log(error));
    },
    getNumberOfPosts() {
        return fetch(this.url)
            .then(response => response.json())
            .then(data => data.length)
            .catch(error => console.log(error));
    }
}





