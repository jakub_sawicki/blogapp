import { createSelector } from 'reselect';

const postsSelector = (state) => state.posts.byId;
const selectedIdsSelector = (state) => state.posts.allIds; 

const getSelectedPosts = (byId, allIds) => {
    const posts = [];
    for (const id of allIds) {
        posts.push(byId[id]);
    }
    return posts;
}

export const selectedPostsSelector = createSelector(
    postsSelector,
    selectedIdsSelector,
    getSelectedPosts
)