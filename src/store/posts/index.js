import { GET_POSTS, GET_NUMBER_OF_POSTS, GET_POST_IDS } from './types';

const initialState = {
    byId: {},
    allIds: [],
    numberOfPosts: 0
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_POSTS: {
            return {
                ...state,
                byId: action.payload.postsById,
                allIds: action.payload.allIds
            }
        }
        case GET_NUMBER_OF_POSTS: {
            return {
                ...state,
                numberOfPosts: action.payload.numberOfPosts
            }
        }
        default: {
            return state
        }
    }
}

export default reducer;